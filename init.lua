--------------------------------------------------------
-- Minetest :: Sentinel Mod v1.0 (sentinel)
--
-- See README.txt for licensing and other information.
-- Copyright (c) 2016-2020, Leslie E. Krause
--
-- ./games/minetest_game/mods/sentinel/init.lua
--------------------------------------------------------

default.STATUS_MODERATOR_KICK = "%s forcibly kicked by %s (given reason: \"%s\")"
default.STATUS_MODERATOR_JAIL = "%s forcibly jailed by %s (given reason: \"%s\")"
default.STATUS_MODERATOR_KILL = "%s forcibly killed by %s (given reason: \"%s\")"

local config = minetest.load_config( )

--------------------
-- Public Methods --
--------------------

default.is_player_mute = function( target_name )
	return not minetest.check_player_privs( target_name, { shout = true } )
end

default.is_player_lock = function( target_name )
	return not minetest.check_player_privs( target_name, { interact = true } )
end

default.is_poweruser = function( name )
        return minetest.check_player_privs( name, { poweruser = true } )
end

default.kick_player = function( source, target, reason )
        -- might want to add reason here and to the log
	if minetest.kick_player( target ) then
		minetest.log( "action", string.format( default.STATUS_MODERATOR_KICK, target, source, reason ) )
		return true
	else
		return false
        end
end

default.jail_player = function( source, target, reason )
	local target_obj = minetest.get_player_by_name( target )

	if target_obj then
		target_obj:setpos( config.jail_pos )
--		minetest.sound_play( "whoosh", { pos = { }, gain = 0.5, max_hear_distance = 10 } )
		minetest.log( "action", string.format( default.STATUS_MODERATOR_JAIL, target, source, reason ) )
		return true
	else
		return false
	end
end

default.kill_player = function( source, target, reason )
	local target_obj = minetest.get_player_by_name( target )

	if target_obj then
		target_obj:set_hp( 0 )
		minetest.log( "action", string.format( default.STATUS_MODERATOR_KILL, target, source, reason ) )
		return true
	else
		return false
	end
end

default.mute_player = function( source, target, is_mute )
	if minetest.auth_table[ target ] then
		local privs = minetest.get_player_privs( target )

-- this needs to to implement is_mute!
		privs.shout = not privs.shout or nil
		minetest.set_player_privs( target, privs )
		minetest.log( "action", source .. ( privs.shout and " granted (shout) privileges to " or " revoked (shout) privileges from " ) .. target )
		return true
	else
		return false
	end
end

default.lock_player = function( source, target, is_lock )
	if minetest.auth_table[ target ] then
		local privs = minetest.get_player_privs( target )

-- this needs to to implement is_lock!
		privs.interact = not privs.interact or nil
		minetest.set_player_privs( target, privs )
		minetest.log( "action", source .. ( privs.interact and " granted (interact) privileges to " or " revoked (interact) privileges from " ) .. target )

		return true
	else
		return false
	end
end

---------------------------
-- Registered Privileges --
---------------------------

minetest.register_privilege( "poweruser", {
        description = "Exempt from all moderation penalties.",
        give_to_singleplayer = true
} )

minetest.register_privilege( "kill", {
        description = "Kill players and avoid jail.",
        give_to_singleplayer = true
} )

minetest.register_privilege( "jail", {
        description = "Send players to jail immediately.",
        give_to_singleplayer = true
} )

minetest.register_privilege( "move", {
        description = "Teleport players back to spawn.",
        give_to_singleplayer = true
} )

------------------------------
-- Registered Chat Commands --
------------------------------

minetest.register_chatcommand( "move", {
        params = "<player_name>",
        description = "Teleport a player to spawn, or move self if none specified.",
        privs = { move = true },
        func = function ( name, param )
		local target_name = param ~= "" and param or name
		local player = minetest.get_player_by_name( target_name )

		if player then
			player:setpos( config.spawn_pos )
			if stairs.is_player_sitting( name ) then
				stairs.unseat_player( name )
			end
			chat_history.add_message( "_infobot", target_name, "You were teleported by " .. name )
			return true, target_name .. " has been teleported."
		else
			return false, "Failed to move " .. target_name .. "."
		end
	end,
} )

minetest.register_chatcommand( "kick", {
	-- NOTE: this was removed from builtin!
        params = "<player_name> [reason]",
        description = "Kick a player, or kick self if none specified.",
        privs = { kick = true },
        func = function( name, param )
		local args = { string.match( string.trim( param ), "^(%S+)%s+\"?(.-)\"?$" ) }
		local target_name = args[ 1 ] or param ~= "" and param or name
		local reason = args[ 2 ] or "none"

                if not default.is_poweruser( target_name ) and default.kick_player( name, target_name, reason ) then
			chat_history.add_message( "_infobot", target_name, "You were kicked by " .. name )
			return true, target_name .. " has been kicked."
		else
			return false, "Failed to kick " .. target_name .. "."
                end
        end,
} )

minetest.register_chatcommand( "jail", {
        params = "<player_name> [reason]",
	description = "Jail a player, or jail self if none specified.",
	privs = { jail = true },
	func = function( name, param )
		local args = { string.match( string.trim( param ), "^(%S+)%s+\"?(.-)\"?$" ) }
		local target_name = args[ 1 ] or param ~= "" and param or name
		local reason = args[ 2 ] or "none"

		if not default.is_poweruser( target_name ) and default.jail_player( name, target_name, reason ) then
			chat_history.add_message( "_infobot", target_name, "You were jailed by " .. name )
			return true, target_name .. " has been jailed."
		else
			return false, "Failed to jail " .. target_name .. "."
		end
	end
} )

minetest.register_chatcommand( "kill", {
        params = "<player_name> [reason]",
	description = "Kill a player, or kill self if none specified.",
	privs = { kill = true },
	func = function( name, param )
		local args = { string.match( string.trim( param ), "^(%S+)%s+\"?(.-)\"?$" ) }
		local target_name = args[ 1 ] or param ~= "" and param or name
		local reason = args[ 2 ] or "none"

		if not default.is_poweruser( target_name ) and default.kill_player( name, target_name, reason ) then
			chat_history.add_message( "_infobot", target_name, "You were killed by " .. name )
			return true, target_name .. " has been killed."
		else
			return false, "Failed to kill " .. target_name .. "."
		end
	end
} )

minetest.register_chatcommand( "mute", {
	description = "Revoke shout privilege from a player.",
	privs = { basic_privs = true },
	func = function( name, param )
		local target_name = param ~= "" and param or name
		if not default.is_poweruser( target_name ) and default.mute_player( name, target_name, true ) then
			chat_history.add_message( "_infobot", target_name, "You were muted by " .. name )
			return true, target_name .. " has been muted."
		else
			return false, "Failed to mute " .. target_name .. "."
		end
	end
} )

minetest.register_chatcommand( "unmute", {
	description = "Grant shout privilege to a player.",
	privs = { basic_privs = true },
	func = function( name, param )
		local target_name = param ~= "" and param or name
		if not default.is_poweruser( target_name ) and default.mute_player( name, target_name, false ) then
			chat_history.add_message( "_infobot", target_name, "You were unmuted by " .. name )
			return true, target_name .. " has been unmuted."
		else
			return false, "Failed to unmute " .. target_name .. "."
		end
	end
} )

minetest.register_chatcommand( "lock", {
	description = "Revoke interact privilege from a player.",
	privs = { basic_privs = true },
	func = function( name, param )
		local target_name = param ~= "" and param or name
		if not default.is_poweruser( target_name ) and default.lock_player( name, target_name, true ) then
			chat_history.add_message( "_infobot", target_name, "You were locked by " .. name )
			return true, target_name .. " has been locked."
		else
			return false, "Failed to lock " .. target_name .. "."
		end
	end
} )

minetest.register_chatcommand( "unlock", {
	description = "Grant interact privilege to a player.",
	privs = { basic_privs = true },
	func = function( name, param )
		local target_name = param ~= "" and param or name
		if not default.is_poweruser( target_name ) and default.lock_player( name, target_name, false ) then
			chat_history.add_message( "_infobot", target_name, "You were unlocked by " .. name )
			return true, target_name .. " has been unlocked."
		else
			return false, "Failed to unlock " .. target_name .. "."
		end
	end
} )
