Sentinel Mod v1.0

Sentinel affords a layer of abstraction for ordinary moderation duties, including a 
simple set of chat commands and privileges.

The following chat commands are made available:

 * /move [player_name]
   Teleport a player to spawn, or move self if none specified.

 * /kick [player_name] "[reason]"
   Kick a player, or kick self if none specified.

 * /jail [player_name] "[reason]"
   Jail a player, or jail self if none specified.

 * /kill [player_name] "[reason]"
   Kill a player, or kill self if none specified.

 * /mute [player_name]
   Revoke shout privilege from a player.

 * /unmute [player_name]
   Grant shout privilege to a player.

 * /lock [player_name]
   Revoke interact privilege from a player.

 * /unlock [player_name]
   Grant interact privilege to a player.

The new 'poweruser' privilege, when granted to a player, will make the player immune
to any of the operations described above.

For programmatic operations, the API of Default has been extended as follows:

   default.is_player_mute( player_name )
   Returns true if the player's 'shout' privilege is revoked

   default.is_player_lock( player_name )
   Returns true if the player's 'interact' privilege is revoked

   default.is_poweruser( player_name )
   Returns true if the player is granted the 'poweruser' privilege

You will also want to modify 'config.lua' to set the locations for spawn and jail.


Repository
----------------------

Browse source code:
  https://bitbucket.org/sorcerykid/sentinel

Download archive:
  https://bitbucket.org/sorcerykid/sentinel/get/master.zip
  https://bitbucket.org/sorcerykid/sentinel/get/master.tar.gz

Dependencies
----------------------

Configuration Panel Mod (required)
  https://bitbucket.org/sorcerykid/config

Installation
----------------------

  1) Unzip the archive into the mods directory of your game.
  2) Rename the config-master directory to "sentinel".
  3) Add "sentinel" as a dependency to any mods using the API.

License of source code
----------------------

The MIT License (MIT)

Copyright (c) 2016-2020, Leslie E. Krause

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

For more details:
https://opensource.org/licenses/MIT
